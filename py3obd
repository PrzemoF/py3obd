#!/usr/bin/env python3
############################################################################
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
# Copyright 2019 Przemo Firszt (przemo@firszt.eu)
#
# This file is part of py3obd
#
# py3obd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# py3obd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with py3obd; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
############################################################################

import collections
import os
import platform
import sys
import threading
import webbrowser

import debug_event

import obd2_codes

import obd_io

import wx

import yaml


ID_ABOUT = 101
ID_EXIT = 110
ID_CONFIG = 500
ID_CLEAR = 501
ID_GETC = 502
ID_RESET = 503
ID_LOOK = 504
ALL_ON = 505
ALL_OFF = 506

ID_DISCONNECT = 507
ID_HELP_ABOUT = 508
ID_HELP_VISIT = 509
ID_HELP_ORDER = 510

# Define notification event for sensor result window
EVT_RESULT_ID = 1000


class ResultEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""

    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_RESULT_ID)
        self.data = data


# event pro aktualizaci DTC tabu
EVT_DTC_ID = 1001


class DTCEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""

    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_DTC_ID)
        self.data = data


# event pro aktualizaci status tabu
EVT_STATUS_ID = 1002


class StatusEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""

    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_STATUS_ID)
        self.data = data


# FIXME Translate
# event pro aktualizaci tests tabu
EVT_TESTS_ID = 1003


class TestEvent(wx.PyEvent):
    """Simple event to carry arbitrary result data."""

    def __init__(self, data):
        """Init Result Event."""
        wx.PyEvent.__init__(self)
        self.SetEventType(EVT_TESTS_ID)
        self.data = data


class MainApp(wx.App):

    # A listctrl which auto-resizes the column boxes to fill
    class MyListCtrl(wx.ListCtrl, wx.lib.mixins.listctrl.ListCtrlAutoWidthMixin):

        def __init__(self, parent, id, pos=wx.DefaultPosition,
                     size=wx.DefaultSize, style=0):
            wx.ListCtrl.__init__(self, parent, id, pos, size, style)
            wx.lib.mixins.listctrl.ListCtrlAutoWidthMixin.__init__(self)

    class SensorProducer(threading.Thread):

        def __init__(self, notify_window, port_name, serial_timeout,
                     serial_reconnect_attempts, _nb):
            self.port_name = port_name
            self.serial_reconnect_attempts = serial_reconnect_attempts
            self.serial_timeout = serial_timeout
            self.port = None
            self.notify_window = notify_window
            self._nb = _nb
            threading.Thread.__init__(self)

        def init_communication(self):
            self.port = obd_io.OBDPort(self.port_name,
                                       self.notify_window,
                                       self.serial_timeout,
                                       self.serial_reconnect_attempts)

            # Cant open serial port
            if self.port.serial_state == 0:
                return None

            self.active = []

            # read supported PIDS
            self.supp = self.port.sensor(0)[1]

            # PID 0 is always supported
            self.active.append(1)

            wx.PostEvent(self.notify_window,
                         ResultEvent([0, 0, "X"]))
            wx.PostEvent(self.notify_window,
                         debug_event.DebugEvent([1, "Communication initialized..."]))

            for i in range(1, len(self.supp)):
                # put X in coloum if PID is supported
                if self.supp[i - 1] == "1":
                    self.active.append(1)
                    wx.PostEvent(self.notify_window,
                                 ResultEvent([i, 0, "X"]))
                else:
                    self.active.append(0)
                    wx.PostEvent(self.notify_window,
                                 ResultEvent([i, 0, ""]))
            return "OK"

        def run(self):
            wx.PostEvent(self.notify_window,
                         StatusEvent([0, 1, "Connecting...."]))
            self.init_communication()
            # cant connect, exit thread
            if self.port.serial_state == 0:
                self.stop()
                # signal apl, that communication was disconnected
                wx.PostEvent(self.notify_window,
                             StatusEvent([666]))
                wx.PostEvent(self.notify_window,
                             StatusEvent([0, 1, "Error cant connect..."]))
                return None

            wx.PostEvent(self.notify_window,
                         StatusEvent([0, 1, "Connected"]))
            wx.PostEvent(self.notify_window,
                         StatusEvent([2, 1, self.port.ELMver]))
            prevstate = -1
            curstate = -1
            while self.notify_window.ThreadControl != 666:
                prevstate = curstate
                curstate = self._nb.GetSelection()
                # show status tab
                if curstate == 0:
                    pass
                # show tests tab
                elif curstate == 1:
                    res = self.port.get_tests_mil()
                    for i in range(0, len(res)):
                        wx.PostEvent(self.notify_window,
                                     TestEvent([i, 1, res[i]]))
                # show sensor tab
                elif curstate == 2:
                    for i in range(3, len(self.active)):
                        if self.active[i]:
                            s = self.port.sensor(i)
                            wx.PostEvent(self.notify_window,
                                         ResultEvent([i, 2, "%s (%s)" % (s[1], s[2])]))
                        if self.notify_window.ThreadControl == 666:
                            break
                # show DTC tab
                elif curstate == 3:
                    # clear DTC
                    if self.notify_window.ThreadControl == 1:
                        self.port.clear_dtc()
                        # before reset ThreadControl we must check if
                        # the main thread did not want us to finish
                        if self.notify_window.ThreadControl == 666:
                            break

                        self.notify_window.ThreadControl = 0
                        # to reread DTC
                        prevstate = -1
                    # reread DTC
                    if self.notify_window.ThreadControl == 2:
                        prevstate = -1

                        if self.notify_window.ThreadControl == 666:
                            break

                        self.notify_window.ThreadControl = 0
                    if prevstate != 3:
                        # clear list
                        wx.PostEvent(self.notify_window, DTCEvent(0))
                        dtc_codes = self.port.get_dtc()
                        if len(dtc_codes) == 0:
                            wx.PostEvent(self.notify_window,
                                         DTCEvent(["", "", "No DTC codes (codes cleared)"]))
                        for i in range(0, len(dtc_codes)):
                            wx.PostEvent(self.notify_window,
                                         DTCEvent([dtc_codes[i][1], dtc_codes[i][0], obd2_codes.pcodes[dtc_codes[i][1]]]))
                else:
                    pass
            self.stop()

        def off(self, id):
            if id >= 0 and id < len(self.active):
                self.active[id] = 0
            else:
                debug("Invalid sensor id")

        def on(self, id):
            if id >= 0 and id < len(self.active):
                self.active[id] = 1
            else:
                debug("Invalid sensor id")

        def all_off(self):
            for i in range(0, len(self.active)):
                self.off(i)

        def all_on(self):
            for i in range(0, len(self.active)):
                self.off(i)

        def stop(self):
            # if stop is called before any connection port is not defined (and not connected )
            if self.port != None:
                self.port.close()
            wx.PostEvent(self.notify_window, StatusEvent([0, 1, "Disconnected"]))
            wx.PostEvent(self.notify_window, StatusEvent([2, 1, "----"]))

  # class producer end

    def sensor_control_on(self):  # after connection enable few buttons
        self.settingmenu.Enable(ID_CONFIG, False)
        self.settingmenu.Enable(ID_RESET, False)
        self.settingmenu.Enable(ID_DISCONNECT, True)
        self.dtcmenu.Enable(ID_GETC, True)
        self.dtcmenu.Enable(ID_CLEAR, True)
        self.GetDTCButton.Enable(True)
        self.ClearDTCButton.Enable(True)

        def sensor_toggle(e):
            sel = e.m_itemIndex
            state = self.senprod.active[sel]
            print(sel, state)
            if state == 0:
                self.senprod.on(sel)
                self.sensors.SetItem(sel, 1, "1")
            elif state == 1:
                self.senprod.off(sel)
                self.sensors.SetItem(sel, 1, "0")
            else:
                debug("Incorrect sensor state")

        self.sensors.Bind(wx.EVT_LIST_ITEM_ACTIVATED, sensor_toggle, id=self.sensor_id)

    def sensor_control_off(self):  # after disconnect disable fer buttons
        self.dtcmenu.Enable(ID_GETC, False)
        self.dtcmenu.Enable(ID_CLEAR, False)
        self.settingmenu.Enable(ID_DISCONNECT, False)
        self.settingmenu.Enable(ID_CONFIG, True)
        self.settingmenu.Enable(ID_RESET, True)
        self.GetDTCButton.Enable(False)
        self.ClearDTCButton.Enable(False)
        # http://pyserial.sourceforge.net/                                                    empty function
        #EVT_LIST_ITEM_ACTIVATED(self.sensors,self.sensor_id, lambda : None)

    def build_sensor_page(self):
        hoffset_list = 0
        t_id = wx.NewIdRef()
        self.sensor_id = t_id
        panel = wx.Panel(self.nb, -1)

        self.sensors = self.MyListCtrl(panel, t_id, pos=wx.Point(0, hoffset_list),
                                       style=wx.LC_REPORT |
                                       wx.SUNKEN_BORDER |
                                       wx.LC_HRULES |
                                       wx.LC_SINGLE_SEL)

        self.sensors.InsertColumn(0, "Supported", width=70)
        self.sensors.InsertColumn(1, "Sensor", format=wx.LIST_FORMAT_RIGHT, width=250)
        self.sensors.InsertColumn(2, "Value")
        for i in range(0, len(obd_io.obd_sensors.SENSORS)):
            s = obd_io.obd_sensors.SENSORS[i].name
            self.sensors.InsertItem(i, "")
            self.sensors.SetItem(i, 1, s)

        ####################################################################
        # This little bit of magic keeps the list the same size as the frame
        def on_psize(e, win=panel):
            panel.SetSize(e.GetSize())
            self.sensors.SetSize(e.GetSize())
            w, h = self.frame.GetClientSize().Get()
            self.sensors.SetSize(0, hoffset_list, w - 10, h - 35)

        panel.Bind(wx.EVT_SIZE, on_psize)
        ####################################################################

        self.nb.InsertPage(2, panel, "Sensors")

    def build_dtc_page(self):
        # offset from the top of panel (space for buttons)
        hoffset_list = 30
        t_id = wx.NewIdRef()
        self.DTCpanel = wx.Panel(self.nb, -1)
        self.GetDTCButton = wx.Button(self.DTCpanel, -1, "Get DTC", wx.Point(15, 0))
        self.ClearDTCButton = wx.Button(self.DTCpanel, -1, "Clear DTC", wx.Point(100, 0))

        # bind functions to button click action
        self.DTCpanel.Bind(wx.EVT_BUTTON, self.get_dtc, self.GetDTCButton)
        self.DTCpanel.Bind(wx.EVT_BUTTON, self.query_clear, self.ClearDTCButton)

        self.dtc = self.MyListCtrl(self.DTCpanel, t_id, pos=wx.Point(0, hoffset_list),
                                   style=wx.LC_REPORT | wx.SUNKEN_BORDER | wx.LC_HRULES | wx.LC_SINGLE_SEL)

        self.dtc.InsertColumn(0, "Code", width=100)
        self.dtc.InsertColumn(1, "Status", width=100)
        self.dtc.InsertColumn(2, "Trouble code")
        ####################################################################
        # This little bit of magic keeps the list the same size as the frame

        def on_psize(e, win=self.DTCpanel):
            self.DTCpanel.SetSize(e.GetSize())
            self.dtc.SetSize(e.GetSize())
            w, h = self.frame.GetClientSize().Get()
            # I have no idea where 70 comes from
            self.dtc.SetSize(0, hoffset_list, w - 16, h - 70)

        self.DTCpanel.Bind(wx.EVT_SIZE, on_psize)
        ####################################################################

        self.nb.InsertPage(3, self.DTCpanel, "DTC")

    def trace_debug(self, level, msg):
        if self.DEBUGLEVEL <= level:
            self.trace.Append([str(level), msg])

    def OnInit(self):
        self.ThreadControl = 0  # say thread what to do
        self.com_port = 0
        self.senprod = None
        self.DEBUGLEVEL = 0  # debug everthing

        t_id = wx.NewId()

        frame = wx.Frame(None, -1, "py3obd-II")
        self.frame = frame

        # Main notebook frames
        self.nb = wx.Notebook(frame, -1, style=wx.NB_TOP)

        self.trace = self.MyListCtrl(self.nb, t_id, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.trace.InsertColumn(0, "Level", width=40)
        self.trace.InsertColumn(1, "Message")
        self.nb.AddPage(self.trace, "Trace")
        self.trace_debug(1, "Initialising application")

        running_sys = platform.system()
        self.trace_debug(1, "System detected: {}".format(running_sys))
        if running_sys == "Linux":
            self.config_file = "~/.py3obd_config.yaml"
        else:
            self.config_file = "py3obd_config.yaml"

        # read settings from file
        try:
            print(self.config_file)
            self.trace_debug(1, "Opening config file: {}".format(self.config_file))
            with open(self.config_file) as f:
                print(f)
                self.config_parameters = yaml.safe_load(f)
                print(self.config_parameters)
                self.com_port = self.config.get("py3obd", "com_port")
                self.serial_reconnect_attempts = self.config.getint("py3obd", "serial_reconnect_attempts")
                self.serial_timeout = self.config.getint("py3obd", "serial_timeout")
        except FileNotFoundError:
            self.config_parameters = None
            self.com_port = "/dev/ttyUSB0"
            self.serial_reconnect_attempts = 5
            self.serial_timeout = 2

        print("config_file: {}".format(self.config_file))
        print("config_parameters: {}".format(self.config_parameters))

        self.Connect(-1, -1, EVT_RESULT_ID, self.on_result)
        self.Connect(-1, -1, debug_event.EVT_DEBUG_ID, self.on_debug)
        self.Connect(-1, -1, EVT_DTC_ID, self.on_dtc)
        self.Connect(-1, -1, EVT_STATUS_ID, self.on_status)
        self.Connect(-1, -1, EVT_TESTS_ID, self.on_tests)

        self.status = self.MyListCtrl(self.nb, t_id, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.status.InsertColumn(0, "Description", width=200)
        self.status.InsertColumn(1, "Value")
        self.status.Append(["Link State", "Disconnnected"])
        self.status.Append(["Protocol", "---"])
        self.status.Append(["Cable version", "---"])
        self.status.Append(["COM port", self.com_port])

        self.nb.InsertPage(0, self.status, "Status", select=True)

        self.OBDTests = self.MyListCtrl(self.nb, t_id, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.OBDTests.InsertColumn(0, "Description", width=200)
        self.OBDTests.InsertColumn(1, "Value")
        self.nb.InsertPage(1, self.OBDTests, "Tests")

        # fill MODE 1 PID 1 test description
        for i in range(0, len(obd2_codes.ptest)):
            self.OBDTests.Append([obd2_codes.ptest[i], "---"])

        self.build_sensor_page()

        self.build_dtc_page()

        self.trace_debug(1, "Application initialised")

        # Setting up the menu.
        self.filemenu = wx.Menu()
        self.filemenu.Append(ID_EXIT, "E&xit", " Terminate the program")

        self.settingmenu = wx.Menu()
        self.settingmenu.Append(ID_CONFIG, "Configure", " Configure py3obd")
        self.settingmenu.Append(ID_RESET, "Connect", " Reopen and connect to device")
        self.settingmenu.Append(ID_DISCONNECT, "Disconnect", "Close connection to device")

        self.dtcmenu = wx.Menu()
        # tady toto nastavi automaticky tab DTC a provede akci
        self.dtcmenu.Append(ID_GETC, "Get DTCs", " Get DTC Codes")
        self.dtcmenu.Append(ID_CLEAR, "Clear DTC", " Clear DTC Codes")
        self.dtcmenu.Append(ID_LOOK, "Code Lookup", " Lookup DTC Codes")

        self.helpmenu = wx.Menu()

        self.helpmenu.Append(ID_HELP_ABOUT, "About this program", " Get DTC Codes")
        self.helpmenu.Append(ID_HELP_VISIT, "Visit program homepage", " Lookup DTC Codes")

        # Creating the menubar.
        self.menuBar = wx.MenuBar()
        self.menuBar.Append(self.filemenu, "&File")  # Adding the "filemenu" to the MenuBar
        self.menuBar.Append(self.settingmenu, "&OBD-II")
        self.menuBar.Append(self.dtcmenu, "&Trouble codes")
        self.menuBar.Append(self.helpmenu, "&Help")

        # Adding the MenuBar to the Frame content.
        frame.SetMenuBar(self.menuBar)

        # attach the menu-event ID_EXIT to the
        frame.Bind(wx.EVT_MENU, self.on_exit, id=ID_EXIT)
        frame.Bind(wx.EVT_MENU, self.query_clear, id=ID_CLEAR)
        frame.Bind(wx.EVT_MENU, self.configure, id=ID_CONFIG)
        frame.Bind(wx.EVT_MENU, self.open_port, id=ID_RESET)
        frame.Bind(wx.EVT_MENU, self.on_disconnect, id=ID_DISCONNECT)
        frame.Bind(wx.EVT_MENU, self.get_dtc, id=ID_GETC)
        frame.Bind(wx.EVT_MENU, self.code_lookup, id=ID_LOOK)
        frame.Bind(wx.EVT_MENU, self.on_help_abouy, id=ID_HELP_ABOUT)
        frame.Bind(wx.EVT_MENU, self.on_help_visit, id=ID_HELP_VISIT)

        self.SetTopWindow(frame)

        frame.Show(True)
        frame.SetSize((520, 400))
        self.sensor_control_off()

        return True

    def on_help_visit(self, event):
        webbrowser.open("https://gitlab.com/PrzemoF/py3obd")

    def on_help_abouy(self, event):  # todo about box
        Text = """ WARNING -  This is a fork!!!
py3obd is an automotive OBD2 diagnosting application using ELM237 cable.

(C) 2008-2009 SeCons Ltd.
(C) 2004 Charles Donour Sizemore

http://www.obdtester.com/
http://www.secons.com/

  py3obd is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the Free Software Foundation; 
either version 2 of the License, or (at your option) any later version.

  py3obd is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MEHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU General Public License for more details. You should have received a copy of 
the GNU General Public License along with PyOBD; if not, write to 
the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

        # HelpAboutDlg = wx.Dialog(self.frame, id, title="About")

        # box  = wx.BoxSizer(wx.HORIZONTAL)
        # box.Add(wx.StaticText(reconnect_panel,-1,Text,pos=(0,0),size=(200,200)))
        # box.Add(wx.Button(HelpAboutDlg,wx.ID_OK),0)
        # box.Add(wx.Button(HelpAboutDlg,wx.ID_CANCEL),1)

        # HelpAboutDlg.SetSizer(box)
        # HelpAboutDlg.SetAutoLayout(True)
        # sizer.Fit(HelpAboutDlg)
        # HelpAboutDlg.ShowModal()

        self.HelpAboutDlg = wx.MessageDialog(self.frame, Text, 'About', wx.OK | wx.ICON_INFORMATION)
        self.HelpAboutDlg.ShowModal()
        self.HelpAboutDlg.Destroy()

    def on_result(self, event):
        self.sensors.SetItem(event.data[0], event.data[1], event.data[2])

    def on_status(self, event):
        if event.data[0] == 666:  # signal, that connection falied
            self.sensor_control_off()
        else:
            self.status.SetItem(event.data[0], event.data[1], event.data[2])

    def on_tests(self, event):
        self.OBDTests.SetItem(event.data[0], event.data[1], event.data[2])

    def on_debug(self, event):
        self.trace_debug(event.data[0], event.data[1])

    def on_dtc(self, event):
        if event.data == 0:  # signal, that DTC was cleared
            self.dtc.DeleteAllItems()
        else:
            self.dtc.Append(event.data)

    def on_disconnect(self, event):  # disconnect connection to ECU
        self.ThreadControl = 666
        self.sensor_control_off()

    def open_port(self, e):

        if self.senprod:  # signal current producers to finish
            self.senprod.stop()
        self.ThreadControl = 0
        self.senprod = self.SensorProducer(self, self.com_port, self.serial_timeout, self.serial_reconnect_attempts, self.nb)
        self.senprod.start()

        self.sensor_control_on()

    def get_dtc(self, e):
        self.nb.SetSelection(3)
        self.ThreadControl = 2

    def add_dtc(self, code):
        self.dtc.InsertItem(0, "")
        self.dtc.SetItem(0, 0, code[0])
        self.dtc.SetItem(0, 1, code[1])

    def code_lookup(self, e=None):
        id = 0
        diag = wx.Frame(None, id, title="Diagnostic Trouble Codes")

        tree = wx.TreeCtrl(diag, id, style=wx.TR_HAS_BUTTONS)

        root = tree.AddRoot("Code Reference")
        proot = root  # tree.AppendItem(root,"Powertrain (P) Codes")
        codes = collections.OrderedDict(sorted(obd2_codes.pcodes.items()))
        group = ""
        for c in codes:
            if c[:3] != group:
                group_root = tree.AppendItem(proot, c[:3] + "XX")
                group = c[:3]
            leaf = tree.AppendItem(group_root, c)
            tree.AppendItem(leaf, obd2_codes.pcodes[c])

        diag.SetSize((400, 500))
        diag.Show(True)

    def query_clear(self, e):
        id = 0
        diag = wx.Dialog(self.frame, id, title="Clear DTC?")

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(wx.StaticText(diag, -1, "Are you sure you wish to"), 0)
        sizer.Add(wx.StaticText(diag, -1, "clear all DTC codes and "), 0)
        sizer.Add(wx.StaticText(diag, -1, "freeze frame data?      "), 0)
        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(diag, wx.ID_OK, "Ok"), 0)
        box.Add(wx.Button(diag, wx.ID_CANCEL, "Cancel"), 0)

        sizer.Add(box, 0)
        diag.SetSizer(sizer)
        diag.SetAutoLayout(True)
        sizer.Fit(diag)
        r = diag.ShowModal()
        if r == wx.ID_OK:
            self.ClearDTC()

    def clear_dtc(self):
        self.ThreadControl = 1
        self.nb.SetSelection(3)

    def scan_serial(self):
        """scan for available ports. return a list of serial names"""
        available = []
        import serial.tools.list_ports
        ports = serial.tools.list_ports.comports()

        for port, desc, hwid in sorted(ports):
                print("{}: {} [{}]".format(port, desc, hwid))
                available.append(format(port))

        # ELM-USB shows up as /dev/tty.usbmodemXXXX, where XXXX is a changing hex string
        # on connection; so we have to search through all 64K options
        if len(platform.mac_ver()[0]) != 0:  # search only on MAC
            for i in range(65535):
                extension = hex(i).replace("0x", "", 1)
                try:
                    s = serial.Serial("/dev/tty.usbmodem" + extension)
                    available.append(s.portstr)
                    s.close()
                except serial.SerialException:
                    pass

        return available

    def configure(self, e=None):
        id = 0
        diag = wx.Dialog(self.frame, id, title="Configure")
        sizer = wx.BoxSizer(wx.VERTICAL)

        ports = self.scan_serial()
        rb = wx.RadioBox(diag, id, "Choose Serial Port",
                         choices=ports, style=wx.RA_SPECIFY_COLS,
                         majorDimension=2)

        sizer.Add(rb, 0)

        # timeOut input control
        timeout_panel = wx.Panel(diag, -1)
        timeout_ctrl = wx.TextCtrl(timeout_panel, -1, '', pos=(140, 0),
                                   size=(35, 25))

        wx.StaticText(timeout_panel, -1, 'Timeout:', pos=(3, 5),
                      size=(140, 20))

        timeout_ctrl.SetValue(str(self.serial_timeout))

        # reconnect attempt input control
        reconnect_panel = wx.Panel(diag, -1)
        reconnect_ctrl = wx.TextCtrl(reconnect_panel, -1, '', pos=(140, 0),
                                     size=(35, 25))

        wx.StaticText(reconnect_panel, -1, 'Reconnect attempts:', pos=(3, 5),
                      size=(140, 20))

        reconnect_ctrl.SetValue(str(self.serial_reconnect_attempts))

        # set actual serial port choice
        if (self.com_port != 0) and (self.com_port in ports):
            rb.SetSelection(ports.index(self.com_port))

        sizer.Add(timeout_panel, 0)
        sizer.Add(reconnect_panel, 0)

        box = wx.BoxSizer(wx.HORIZONTAL)
        box.Add(wx.Button(diag, wx.ID_OK), 0)
        box.Add(wx.Button(diag, wx.ID_CANCEL), 1)

        sizer.Add(box, 0)
        diag.SetSizer(sizer)
        diag.SetAutoLayout(True)
        sizer.Fit(diag)
        r = diag.ShowModal()
        if r == wx.ID_OK:
            try:
                self.com_port = ports[rb.GetSelection()]
                print(self.com_port)
                self.write_config()
            except wx._core.wxAssertionError:
                print("No port selected")

    def write_config(self):
        storage = {}
        storage["com_port"] = self.com_port
        storage["serial_timeout"] = self.serial_timeout
        storage["serial_reconnect_attempts"] = self.serial_reconnect_attempts
        fv = yaml.dump(storage, default_flow_style=False, allow_unicode=True)
        full_config_path = os.path.expanduser(self.config_file)
        # Make sure file exists
        open(full_config_path, "a").close()
        f = open(full_config_path, "r+")
        f.write(fv)
        f.close()

    def on_exit(self, e=None):
        sys.exit(0)

app = MainApp(0)
app.MainLoop()
