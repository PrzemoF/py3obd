#!/usr/bin/env python3
###########################################################################
# obd_sensors.py
#
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of py3obd.
#
# py3obd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# py3obd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with py3obd; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
###########################################################################


def hex_to_int(input_string):
    ret = 0
    try:
        ret = int(input_string, 16)
    except ValueError:
        print("Can't convert string \"{}\" to hex, defaulting to 0".format(input_string))
    return ret


def maf(code):
    code = hex_to_int(code)
    return code * 0.00132276


def throttle_pos(code):
    code = hex_to_int(code)
    return code * 100.0 / 255.0


def intake_m_pres(code):
    # in kPa
    code = hex_to_int(code)
    return code / 0.14504


def rpm(code):
    code = hex_to_int(code)
    return code / 4


def speed(code):
    code = hex_to_int(code)
    return code / 1.609


def percent_scale(code):
    code = hex_to_int(code)
    return code * 100.0 / 255.0


def timing_advance(code):
    code = hex_to_int(code)
    return (code - 128) / 2.0


def sec_to_min(code):
    code = hex_to_int(code)
    return code / 60


def temp(code):
    code = hex_to_int(code)
    return code - 40


def cpass(code):
    # FIXME
    return code


def fuel_trim_percent(code):
    code = hex_to_int(code)
    return (code - 128.0) * 100.0 / 128


def dtc_decrypt(code):
    # first byte is byte after PID and without spaces
    # A byte
    num = hex_to_int(code[:2])
    res = []
    # is MIL light on
    if num & 0x80:
        mil = 1
    else:
        mil = 0

    # bit 0-6 are the number of dtc's.
    num = num & 0x7f

    res.append(num)
    res.append(mil)
    # B byte
    num_b = hex_to_int(code[2:4])

    for i in range(0, 3):
        res.append(((num_b >> i) & 0x01)+((num_b >> (3+i)) & 0x02))
    # C byte
    num_c = hex_to_int(code[4:6])
    # D byte
    num_d = hex_to_int(code[6:8])

    for i in range(0, 7):
        res.append(((num_c >> i) & 0x01)+(((num_d >> i) & 0x01) << 1))

    # EGR SystemC7  bit of different
    res.append(((num_d >> 7) & 0x01))

    return res


def hex_to_bitstring(input_string):
    input_integer = hex_to_int(input_string)
    bitstring = f"{input_integer:0>16b}"
    return bitstring


class Sensor:
    def __init__(self, sensor_name, sensor_command, sensor_val_function, unit):
        self.name = sensor_name
        self.cmd = sensor_command
        self.value = sensor_val_function
        self.unit = unit


SENSORS = [
    Sensor("SupportedPIDs", "0100", hex_to_bitstring, ""),
    Sensor("StatusSinceDTCCleared", "0101", dtc_decrypt, ""),
    Sensor("DTCCausingFreezeFrame", "0102", cpass, ""),
    Sensor("FuelSystemStatus", "0103", cpass, ""),
    Sensor("CalculatedLoadValue", "0104", percent_scale, ""),
    Sensor("CoolantTemperature", "0105", temp, "C"),
    Sensor("ShortTermFuelTrim", "0106", fuel_trim_percent, "%"),
    Sensor("LongTermFuelTrim", "0107", fuel_trim_percent, "%"),
    Sensor("ShortTermFuelTrim", "0108", fuel_trim_percent, "%"),
    Sensor("LongTermFuelTrim", "0109", fuel_trim_percent, "%"),
    Sensor("FuelRailPressure", "010A", cpass, ""),
    Sensor("IntakeManifoldPressure", "010B", intake_m_pres, "psi"),
    Sensor("EngineRPM", "010C", rpm, ""),
    Sensor("VehicleSpeed", "010D", speed, "MPH"),
    Sensor("TimingAdvance", "010E", timing_advance, "degrees"),
    Sensor("IntakeAirTemp", "010F", temp, "C"),
    Sensor("AirFlowRate(MAF)", "0110", maf, "lb/min"),
    Sensor("ThrottlePosition", "0111", throttle_pos, "%"),
    Sensor("SecondaryAirStatus", "0112", cpass, ""),
    Sensor("LocationofO2sensors", "0113", cpass, ""),
    Sensor("O2Sensor:1-1", "0114", fuel_trim_percent, "%"),
    Sensor("O2Sensor:1-2", "0115", fuel_trim_percent, "%"),
    Sensor("O2Sensor:1-3", "0116", fuel_trim_percent, "%"),
    Sensor("O2Sensor:1-4", "0117", fuel_trim_percent, "%"),
    Sensor("O2Sensor:2-1", "0118", fuel_trim_percent, "%"),
    Sensor("O2Sensor:2-2", "0119", fuel_trim_percent, "%"),
    Sensor("O2Sensor:2-3", "011A", fuel_trim_percent, "%"),
    Sensor("O2Sensor:2-4", "011B", fuel_trim_percent, "%"),
    Sensor("OBDDesignation", "011C", cpass, ""),
    Sensor("LocationofO2sensors", "011D", cpass, ""),
    Sensor("Auxinputstatus", "011E", cpass, ""),
    Sensor("TimeSinceEngineStart", "011F", sec_to_min, "min"),
    Sensor("EngineRunwithMILon", "014E", sec_to_min, "min"),

    ]


def self_test():
    for i in SENSORS:
        print("name: {}\n cmd: {}\nvalue:{}\nunit:{}".format(i.name,
                                                             i.cmd,
                                                             i.value("F"),
                                                             i.unit))


if __name__ == "__main__":
    self_test()
