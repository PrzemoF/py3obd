#!/usr/bin/env python3
###########################################################################
# odb_io.py
#
# Copyright 2004 Donour Sizemore (donour@uchicago.edu)
# Copyright 2009 Secons Ltd. (www.obdtester.com)
#
# This file is part of py3obd.
#
# py3obd is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# py3obd is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with py3obd; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
###########################################################################

import string
import time

import debug_event

import obd_sensors

import serial

import wx  # due to debugEvent messaging
import wx.lib.mixins.listctrl

GET_DTC_COMMAND = "03"
CLEAR_DTC_COMMAND = "04"
GET_FREEZE_DTC_COMMAND = "07"


def decrypt_dtc_code(code):
    """Returns the 5-digit DTC code from hex encoding"""
    dtc = []
    current = code
    for i in range(0, 3):
        if len(current) < 4:
            raise "Tried to decode bad DTC: %s" % code

        tc = obd_sensors.hex_to_int(current[0])  # typecode
        tc = tc >> 2
        dtc_types = "PCBU"
        try:
            dtc_type = dtc_types[tc]
        except IndexError:
            print("Unknown typecode: {}".format(tc))
            raise

        # if tc == 0:1
        #     dtc_type = "P"
        # elif tc == 1:
        #     dtc_type = "C"
        # elif tc == 2:
        #     dtc_type = "B"
        # elif tc == 3:
        #     dtc_type = "U"
        # else:
        #     raise tc

        dig1 = str(obd_sensors.hex_to_int(current[0]) & 3)
        dig2 = str(obd_sensors.hex_to_int(current[1]))
        dig3 = str(obd_sensors.hex_to_int(current[2]))
        dig4 = str(obd_sensors.hex_to_int(current[3]))
        dtc.append(dtc_type + dig1 + dig2 + dig3 + dig4)
        current = current[4:]
    return dtc


class OBDPort:
    """ OBDPort abstracts all communication with OBD-II device."""

    def __init__(self, portnum, notify_window, serial_timeout, no_of_reconnects):
        """Initializes port by resetting device and gettings supported PIDs. """
        # These should really be set by the user.
        baud = 9600
        databits = 8
        par = serial.PARITY_NONE  # parity
        sb = 1                   # stop bits
        self.port = None
        self.ELMver = "Unknown"
        self.serial_state = 1  # state SERIAL is 1 connected, 0 disconnected (connection failed)

        self.notify_window = notify_window
        wx.PostEvent(self.notify_window, debug_event.DebugEvent([1, "Opening interface (serial port)"]))
        # Connect in init is not a good idea
        try:
            self.port = serial.Serial(portnum, baud,
                                      parity=par, stopbits=sb, bytesize=databits, timeout=serial_timeout)

        except serial.SerialException:
            # TODO show detailed error
            print("Error, can't connect, {} {} {} {} {} {}".format(portnum, baud, par, sb, databits, serial_timeout))
            self.serial_state = 0
            return None

        wx.PostEvent(self.notify_window, debug_event.DebugEvent([1, "Interface successfully " + self.port.portstr + " opened"]))
        wx.PostEvent(self.notify_window, debug_event.DebugEvent([1, "Connecting to ECU..."]))

        count = 0
        while 1:  # until error is returned try to connect
            try:
                self.send_command("atz")   # initialize
            except serial.SerialException:
                self.serial_state = 0
                return None

            self.ELMver = self.get_result()
            wx.PostEvent(self.notify_window, debug_event.DebugEvent([2, "atz response:" + self.ELMver]))
            self.send_command("ate0")  # echo off
            wx.PostEvent(self.notify_window, debug_event.DebugEvent([2, "ate0 response:" + self.get_result()]))
            self.send_command("0100")
            ready = self.get_result()
            wx.PostEvent(self.notify_window, debug_event.DebugEvent([2, "0100 response1:" + ready]))
            if ready == "BUSINIT: ...OK":
                ready = self.get_result()
                wx.PostEvent(self.notify_window, debug_event.DebugEvent([2, "0100 response2:" + ready]))
                return None
            else:
                # ready=ready[-5:] #Expecting error message: BUSINIT:.ERROR (parse last 5 chars)
                wx.PostEvent(self.notify_window, debug_event.DebugEvent([2, "Connection attempt failed:" + ready]))
                time.sleep(5)
                if count == no_of_reconnects:
                    self.close()
                    self.serial_state = 0
                    return None
                wx.PostEvent(self.notify_window, debug_event.DebugEvent([2, "Connection attempt:" + str(count)]))
                count = count + 1

    def close(self):
        """ Resets device and closes all associated filehandles"""

        if self.port is not None and self.serial_state == 1:
            self.send_command("atz")
            self.port.close()

        self.port = None
        self.ELMver = "Unknown"

    def send_command(self, cmd):
        """Internal use only: not a public interface"""
        if self.port:
            self.port.flushOutput()
            self.port.flushInput()
            for c in cmd:
                self.port.write(c.encode())
            self.port.write("\r\n".encode())
            wx.PostEvent(self.notify_window, debug_event.DebugEvent([3, "Send command:" + cmd]))

    def interpret_result(self, code):
        """Internal use only: not a public interface"""
        # Code will be the string returned from the device.
        # It should look something like this:
        # '41 11 0 0\r\r'

        # 9 seems to be the length of the shortest valid response
        if len(code) < 7:
            raise "BogusCode"

        # get the first thing returned, echo should be off
        code = string.split(code, "\r")
        code = code[0]

        # remove whitespace
        code = string.split(code)
        code = string.join(code, "")

        # cables can behave differently
        if code[:6] == "NODATA":  # there is no such sensor
            return "NODATA"

        # first 4 characters are code from ELM
        code = code[4:]
        return code

    def get_result(self):
        """Internal use only: not a public interface"""
        time.sleep(0.1)
        if self.port:
            buffer = ""
            while 1:
                c = self.port.read(1)
                if c == '\r' and len(buffer) > 0:
                    break
                else:
                    if buffer != "" or c != ">":  # if something is in buffer, add everything
                        print(c)
                        buffer = buffer + c.decode()
            wx.PostEvent(self.notify_window, debug_event.DebugEvent([3, "Get result:" + buffer]))
            return buffer
        else:
            wx.PostEvent(self.notify_window, debug_event.DebugEvent([3, "NO self.port!" + buffer]))
        return None

    # get sensor value from command
    def get_sensor_value(self, sensor):
        """Internal use only: not a public interface"""
        cmd = sensor.cmd
        self.send_command(cmd)
        data = self.get_result()

        if data:
            data = self.interpret_result(data)
            if data != "NODATA":
                data = sensor.value(data)
        else:
            return "NORESPONSE"
        return data

    # return string of sensor name and value from sensor index
    def sensor(self, sensor_index):
        """Returns 3-tuple of given sensors. 3-tuple consists of
        (Sensor Name (string), Sensor Value (string), Sensor Unit (string) ) """
        sensor = obd_sensors.SENSORS[sensor_index]
        r = self.get_sensor_value(sensor)
        return (sensor.name, r, sensor.unit)

    def sensor_names(self):
        """Internal use only: not a public interface"""
        names = []
        for s in obd_sensors.SENSORS:
            names.append(s.name)
        return names

    def get_tests_mil(self):
        status_text = ["Unsupported", "Supported - Completed", "Unsupported", "Supported - Incompleted"]

        status_res = self.sensor(1)[1]  # GET values
        status_trans = []  # translate values to text

        status_trans.append(str(status_res[0]))  # DTCs

        if status_res[1] == 0:  # MIL
            status_trans.append("Off")
        else:
            status_trans.append("On")

        for i in range(2, len(status_res)):  # Tests
            status_trans.append(status_text[status_res[i]])

        return status_trans

    #
    # fixme: j1979 specifies that the program should poll until the number
    # of returned DTCs matches the number indicated by a call to PID 01
    #
    def get_dtc(self):
        """Returns a list of all pending DTC codes. Each element consists of
        a 2-tuple: (DTC code (string), Code description (string) )"""
        dtc_letters = ["P", "C", "B", "U"]
        r = self.sensor(1)[1]  # data
        dtc_number = r[0]
        mil = r[1]
        dtc_codes = []

        print("Number of stored DTC:" + str(dtc_number) + " MIL: " + str(mil))
        # get all DTC, 3 per mesg response
        for i in range(0, ((dtc_number + 2) / 3)):
            self.send_command(GET_DTC_COMMAND)
            res = self.get_result()
            print("DTC result:" + res)
            for i in range(0, 3):
                val1 = obd_sensors.hex_to_int(res[3 + i * 6:5 + i * 6])
                # get DTC codes from response (3 DTC each 2 bytes)
                val2 = obd_sensors.hex_to_int(res[6 + i * 6:8 + i * 6])
                val = (val1 << 8) + val2  # DTC val as int

                if val == 0:  # skip fill of last packet
                    break

                dtc_str = dtc_letters[(val & 0xC000) > 14] + str((val & 0x3000) >> 12) + str(val & 0x0fff)

                dtc_codes.append(["Active", dtc_str])

        # read mode 7
        self.send_command(GET_FREEZE_DTC_COMMAND)
        res = self.get_result()

        if res[:7] == "NO DATA":  # no freeze frame
            return dtc_codes

        print("DTC freeze result:" + res)
        for i in range(0, 3):
            val1 = obd_sensors.hex_to_int(res[3 + i * 6:5 + i * 6])
            # get DTC codes from response (3 DTC each 2 bytes)
            val2 = obd_sensors.hex_to_int(res[6 + i * 6:8 + i * 6])
            val = (val1 << 8) + val2  # DTC val as int

            if val == 0:  # skip fill of last packet
                break

            dtc_str = dtc_letters[(val & 0xC000) > 14] + str((val & 0x3000) >> 12) + str(val & 0x0fff)
            dtc_codes.append(["Passive", dtc_str])

        return dtc_codes

    def clear_dtc(self):
        """Clears all DTCs and freeze frame data"""
        self.send_command(CLEAR_DTC_COMMAND)
        r = self.get_result()
        return r

    def log(self, sensor_index, filename):
        file = open(filename, "w")
        start_time = time.time()
        if file:
            data = self.sensor(sensor_index)
            file.write("%s     \t%s(%s)\n" %
                       ("Time", string.strip(data[0]), data[2]))
            while 1:
                now = time.time()
                data = self.sensor(sensor_index)
                line = "%.6f,\t%s\n" % (now - start_time, data[1])
                file.write(line)
                file.flush()
